# fake_host

This package have two ways to send OSOTB request to an OSOTB endpoint:

* Through a serial port using 'osotb_serial.py' or
* Through a TCP/IP socket using 'osotb_client.py'

## Guide to use as a package

1. Clone the repository:

`git clone https://gitlab.com/os-otb/fake_host.git`

2. Go to the root dir of the directory and install the package with `pip`:

`pip install .`

3. In any python script you want to use, import the OSOTB fake host in this way:

For osotb serial:

```python
from osotb_fake_host.osotb_serial import OSOTB_Serial

osotb_serial = OSOTB_Serial('/dev/ttyUSB0') ## create the object
pkt = osotb_serial.create_osotb_req_packet(method='get', resource='uptime')
osotb_serial.send_request(osotb_packet=pkt) ## send the request, the response will be printed to stdout
```

For osotb client:

```python
from osotb_fake_host.osotb_client import OSOTB_TCPIP

osotb_tcpip = OSOTB_TCPIP(ip = '192.168.0.83', port = 49253)
osotb_packet = osotb_tcpip.create_osotb_req_packet(resource='uptime', method='GET')
osotb_tcpip.send_request(osotb_packet)
```

## Guide to use as a standalone script

For osotb serial:
```bash
python3 -m osotb_fake_host.osotb_serial --method GET --resource uptime --port /dev/ttyUSB0
```

For osotb client:
```bash
python3 -m osotb_fake_host.osotb_client --method GET --resource uptime --port 49253 --ip 192.168.0.83
```