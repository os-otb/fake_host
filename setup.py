from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="osotb_fake_host",
    version="1.0.0",
    author="Julian Rodriguez",
    author_email="jnrodriguezz@hotmail.com",
    description="A simple OSOTB request sender through TCP/IP sockets or through UART.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/os-otb/fake_host",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: UNIX Based",
    ],
    packages = find_packages(),
    install_requires=[  'pyserial==3.5' ],
    python_requires=">=3.6",
)
