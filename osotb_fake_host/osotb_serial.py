"""
 *  OSOTB serial CLI. With this CLI we can make OSOTB requests to a server through a serial interface.
 *  Example: python3 osotb_serial.py -r UPTIME -m GET -p /dev/ttyUSB0
 *  
 *  Created on: Oct 10, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 *
"""
import serial
import argparse
import logging
import json
from time import monotonic
from binascii import crc_hqx 
from .osotb_parser import OSOTBParser

SERIAL_OSOTB_READ_TIMEOUT_S = 20 ## Timeout or each read (block time on serial.read() function)
SERIAL_OSOTB_WAIT_UNTIL_RESPONSE_S = 60 ## Wait until an osotb response is received

## OSOTB Serial protocol definitions 
SERIAL_OSOTB_MAX_TIME_WITHOUT_NACK_ACK_S = 10 ## Max time that a sender can wait for a NACK or an ACK from the receiver
STX = '\x02'    ## The start of frame of the osotb_serial packets
ACK = '\x06'    ## ACK byte
NACK = '\x15'   ## NACK byte

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='[%Y/%m/%d %I:%M:%S %p]', level=logging.DEBUG)
logger = logging.getLogger(__name__)

class OSOTB_Serial(OSOTBParser):
    """
        A class that send OSOTB request through a serial port.
    """
    def __init__(self, dev_path):
        self.dev_path = dev_path

    def __listen_until_response_is_received(self, timeout_s, serial):
        """
            Listen to the serial port until an osotb_response is received or until
            the timeout has been reached.
            @param timeout the function will return if this timeout has been reached.
            @param serial the serial port object
            @return the osotb response or None if the timeout has been reached.
        """
        osotb_response = None

        t0 = monotonic()
        while (monotonic() - t0) < timeout_s:
            received_crc = bytearray()
            received_byte = serial.read()
            logger.info("received byte = {}".format(received_byte))
            if received_byte == bytes(STX, encoding='utf-8'):
                ## start receiving osotb packet
                osotb_response = serial.readline()
                for i in range(2):
                    received_crc.extend(serial.read())
                received_crc = bytes(received_crc)
                received_crc = hex((received_crc[0]<<8) | (received_crc[1]<<0))
                logger.info('osotb-packet: {}{}'.format(osotb_response, received_crc))
                ## verify crc:
                calculated_crc = hex(crc_hqx(osotb_response[:-1],0)) ## don't count the newline!
                if( calculated_crc == received_crc ):
                    logger.info('crc calculated ({}) its equal to the received ({}), sending ACK'.format(calculated_crc, received_crc))
                    serial.write(bytes(ACK, encoding='utf-8'))
                else:
                    logger.info('crc calculated ({}) its NOT equal to the received ({}), sending ACK'.format(calculated_crc, received_crc))
                    serial.write(bytes(NACK, encoding='utf-8'))
                break

            if (monotonic() - t0) > timeout_s:
                logger.error("timeout reached! ({}s)".format(timeout_s))

        return osotb_response 

    def __create_osotb_serial_packet(self, osotb_packet_data):
        """
            Create an osotb serial packet from an osotb data packet. This method
            adds the header and the trailer to the osotb data packet to create an
            osotb serial packet.
            osotb data packet = json 
            osotb serial packet = header + json + \n + crc
        """
        crc = hex(crc_hqx(osotb_packet_data[:-1].encode('utf-8'), 0)) ## exclude the newline delimiter from the crc!!
        data = osotb_packet_data.encode('utf-8')
        osotb_serial_packet = bytes(STX, encoding='utf-8')+data+bytes.fromhex((crc)[2:])
        logger.info("data: {}, crc: {}".format(data, crc))
        return osotb_serial_packet

    def __send_osotb_serial_packet(self, packet, serial):
        """
            Send a packet and wait until a NACK or an ACK is received from
            the serial port. In each 'send_packet' wait up to 10 seconds for 
            a response. If a response is not received in 10 seconds then increment
            the send error counter. The send error counter will be incremented if a 
            NACK is received too.
            If there are 10 or more send errors then return with error.
            @param packet the packet to send.
            @param serial the serial port object.
            @return True if an ACK is received or False if there was
            10 or more send errors. 
        """
        ack_received = False
        send_errors = 0
        while( (ack_received == False) and (send_errors < 10) ):
            serial.write(packet)
            t0 = monotonic()
            now = t0
            while (now - t0) < SERIAL_OSOTB_MAX_TIME_WITHOUT_NACK_ACK_S:
                now = monotonic()
                received_byte = serial.read()
                if received_byte == bytes(ACK, encoding='utf-8'):
                    logger.info("ack received!")
                    ack_received = True
                elif received_byte == bytes(NACK, encoding='utf-8'):
                    send_errors += 1
                    logger.info('nack received ({})! resending... '.format(send_errors))
                    break
            if (now - t0) > SERIAL_OSOTB_MAX_TIME_WITHOUT_NACK_ACK_S:
                send_errors += 1
                logger.info('{} seconds without NACK or ACK, resend packet!'.format(SERIAL_OSOTB_MAX_TIME_WITHOUT_NACK_ACK_S))
        return ack_received

    def send_request(self, osotb_packet):
        """
            Send an OSOTB request to a server through a serial iface.
            @param dev_path the serial device interface device path (i.e. "/dev/ttyUSB0")
            @param osotb_packet must be the json of the OSOTB request.
        """
        osotb_serial_packet = self.__create_osotb_serial_packet(osotb_packet)
        logger.info('raw packet: {}'.format(osotb_serial_packet))

        logger.info("opening {}".format(self.dev_path))
        ser = serial.Serial(self.dev_path, baudrate=115200, timeout=SERIAL_OSOTB_READ_TIMEOUT_S)

        if ser.is_open:
            logger.info("using {} at {}".format(ser.name, ser.baudrate))
            if self.__send_osotb_serial_packet(packet=osotb_serial_packet, serial=ser):
                response = self.__listen_until_response_is_received(timeout_s = SERIAL_OSOTB_WAIT_UNTIL_RESPONSE_S, serial = ser)
            ser.close()
            if not ser.is_open:
                logger.info("serial port {} was closed successfully".format(ser.name))
            else:
                logger.error("error closing serial port {}".format(ser.name))
        else:
            logger.error("can't open {}".format(ser.name))

def parse_arguments():
    parser = argparse.ArgumentParser(description='Send OSOTB request to a server.')
    parser.add_argument('-r', '--resource',type=str, help='the resource to make a request')
    parser.add_argument('-md', '--method',type=str, help='method used on the resource')
    parser.add_argument('-pay', '--payload', type=str, help='payload of the request')
    parser.add_argument('-p', '--port', type=str, help='serial port device path')
    return parser.parse_args()

def cli():
    args = parse_arguments()
    if (args.resource is None) or (args.method is None) or (args.port is None):
        logging.error("missing arguments (resource={}, method={}, port={}).".format(args.resource, args.method, args.port))

    osotb_serial = OSOTB_Serial(args.port)
    osotb_packet = osotb_serial.create_osotb_req_packet(resource=args.resource, method=args.method, payload=args.payload)
    osotb_serial.send_request(osotb_packet=osotb_packet)

if __name__ == "__main__":
    cli()